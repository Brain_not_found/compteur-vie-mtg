﻿// Using system
using System;

// Using Xamarin
using Xamarin.Essentials;
using Xamarin.Forms;

// Using Project
using CompteurVieMtg.Interfaces;

namespace CompteurVieMtg.Utilities
{
    public class SecureStorageManager
    {
        /// <summary>
        /// Retourne en JSON une valeur récupérée de SecureStorage
        /// </summary>
        /// <returns>string au format JSON</returns>
        public static async System.Threading.Tasks.Task<string> GetFromSecureStorageAsync(string storageKey)
        {
            string stringReturn = null;

            try
            {
                stringReturn = await SecureStorage.GetAsync(storageKey);
                // stringReturn = JsonConvert.SerializeObject(dataFromStorgae);  // Au cas où le string récupéré n'est pas au format Json
            }
            catch (Exception ex)
            {
                DependencyService.Get<IMessage>().LongAlert("Erreur lors de la récupération : " + ex.Message);
            }

            // Ne jamais renvoyer null
            if (stringReturn == null)
            {
                stringReturn = string.Empty;
            }

            return stringReturn;
        }

        /// <summary>
        /// Sauvegarde une valeur ou un objet contenu dans un string au format JSON sous un clé renseignée et renvoie un booléen représentant le résultat de l'action
        /// </summary>
        /// <param name="storageKey"></param>
        /// <param name="jsonObject"></param>
        /// <returns>booléen représentatif du succés de la fonction</returns>
        public static async System.Threading.Tasks.Task<bool> SetToSecureStorageAsync(string storageKey, string jsonObject)
        {
            bool success;

            try
            {
                await SecureStorage.SetAsync(storageKey, jsonObject);
                success = true;
            }
            catch (Exception ex)
            {
                DependencyService.Get<IMessage>().LongAlert("Erreur lors de l'enregistrement : " + ex.Message);
                success = false;
            }

            return success;
        }
    }
}