﻿namespace CompteurVieMtg.Models
{
    public class MtgColor
    {
        public string ColorName { get; set; }
        public string HexCodeColor { get; set; }

        public MtgColor(string colorName)
        {
            this.ColorName = colorName;

            // Ajout du code Hexadécimal selon le nom de couleur
            switch (this.ColorName)
            {
                case "White":
                    this.HexCodeColor = "#FFFFFF";
                    break;
                case "Blue":
                    this.HexCodeColor = "#0000FF";
                    break;
                case "Red":
                    this.HexCodeColor = "#FF0000";
                    break;
                case "Black":
                    this.HexCodeColor = "#000000";
                    break;
                case "Green":
                    this.HexCodeColor = "#34C924";
                    break;
            }
        }
    }
}
