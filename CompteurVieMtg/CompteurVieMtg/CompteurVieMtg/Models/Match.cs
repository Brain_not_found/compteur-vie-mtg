﻿// Using system
using System;
using System.Collections.Generic;

// Using Project
using CompteurVieMtg.Utilities;

// Using package
using Newtonsoft.Json;

namespace CompteurVieMtg.Models
{
    public class Match
    {
        public List<Player> Players { get; set; }
        public int PlayerNumber { get; set; }

        public Match()
        {
            SetPlayers();
        }

        // Définit le nombre de joueurs selon l'info enregistrée dans le stockage interne
        public async void SetPlayers()
        {
            string stockedMatch = await SecureStorageManager.GetFromSecureStorageAsync("stockedMatch");

            // Si un match précédent a été récupéré
            if (stockedMatch != "")
            {
                Match stockedMatchConverted = JsonConvert.DeserializeObject<Match>(stockedMatch);

                this.PlayerNumber = stockedMatchConverted.PlayerNumber;
                this.Players = stockedMatchConverted.Players;
            }
            // Sinon
            else
            {
                string stockedPlayerNumber = await SecureStorageManager.GetFromSecureStorageAsync("stockedPlayerNumber");

                // Initialisation du nombre de joueurs
                if (stockedPlayerNumber == "")
                    this.PlayerNumber = 2;
                else
                    this.PlayerNumber = Int32.Parse(stockedPlayerNumber);

                // Initialisation des joueurs
                this.Players = new List<Player>() { };

                for (int i = 0; i < this.PlayerNumber; i++)
                {
                    this.Players.Add(new Player(i + 1));
                }
            }
        }
    }
}
