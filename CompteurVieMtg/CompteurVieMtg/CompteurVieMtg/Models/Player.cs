﻿// Using system
using System.Collections.Generic;

namespace CompteurVieMtg.Models
{
    public class Player
    {
        public List<MtgColor> UsedColors { get; set; }
        public int LifePoints { get; set; }
        public int PoisonCounter { get; set; }
        public int EnergyCounter { get; set; }
        public int ExperienceCounter { get; set; }
        public bool PoisonActivted { get; set; }
        public bool EnergyActivated { get; set; }
        public bool ExperienceActivated { get; set; }
        public string PlayerName { get; set; }

        public Player(int playerNo)
        {
            this.LifePoints = 20;
            this.PoisonCounter = 0;
            this.EnergyCounter = 0;
            this.ExperienceCounter = 0;
            this.PlayerName = "Player" + playerNo.ToString();
        }
    }
}
