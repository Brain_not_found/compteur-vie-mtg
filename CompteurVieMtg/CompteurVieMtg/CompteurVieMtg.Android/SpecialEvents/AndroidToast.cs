﻿// Using Android
using Android.App;
using Android.Widget;

// Using Project
using CompteurVieMtg.Interfaces;

[assembly : Xamarin.Forms.Dependency(typeof(CompteurVieMtg.Droid.SpecialEvents.AndroidToast))]
namespace CompteurVieMtg.Droid.SpecialEvents
{
    public class AndroidToast : IMessage
    {
        public void LongAlert(string message)
        {
            Toast.MakeText(Application.Context, message, ToastLength.Long).Show();
        }

        public void ShortAlert(string message)
        {
            Toast.MakeText(Application.Context, message, ToastLength.Short).Show();
        }
    }
}